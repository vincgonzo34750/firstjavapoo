package package_cardgame_model;

import package_cardgame_model.Deck;
import package_cardgame_model.PlayingCard;
import package_cardgame_model.Rank;
import package_cardgame_model.Suit;

import java.util.ArrayList;

public class NormalDeck extends Deck {
    public NormalDeck(){
        cards = new ArrayList<PlayingCard>();
        for (Rank rank : Rank.values()){
            for (Suit suit : Suit.values()){
                cards.add(new PlayingCard(rank, suit));
            }
        }

        shuffle();
    }
}
