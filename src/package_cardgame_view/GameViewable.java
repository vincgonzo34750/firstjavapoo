package package_cardgame_view;

import package_cardgame_controller.GameController;

public interface GameViewable {
    void setController(GameController controller);
    void promptForPlayerName();
    void promptForFlip();
    void promptForNewGame();
    void showPlayerName(int playerIndex, String playerName);
    void showFaceDownCardForPlayer(int playerIndex, String playerName);
    void showCardForPlayer(int playerIndex, String playerName, String rank, String suit);
    void showWinner(String playerName);
}
