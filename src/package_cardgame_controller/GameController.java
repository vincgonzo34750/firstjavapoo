package package_cardgame_controller;

import package_cardgame_games_logique.GameEvaluator;
import package_cardgame_model.Deck;
import package_cardgame_model.Player;
import package_cardgame_model.PlayingCard;
import package_cardgame_view.CommandLineView;
import package_cardgame_view.GameSwing;

import java.util.ArrayList;


public class GameController {
    enum GameState {
        AddingPlayers,
        CardsDealt,
        WinnerRevealed
    }

    Deck deck;
    ArrayList<Player> players;
    Player winner;
    GameSwing commandLineView;
    GameState gameState;
    GameEvaluator evaluator;

    public GameController(GameSwing commandLineView, Deck deck, GameEvaluator evaluator){
        this.commandLineView = commandLineView;
        this.deck = deck;
        players = new ArrayList<Player>();
        gameState = GameState.AddingPlayers;
        this.evaluator = evaluator;
        commandLineView.setController(this);
    }

    public void run(){
        while (true){
            switch (gameState){
                case AddingPlayers:
                    commandLineView.promptForPlayerName();
                    break;
                case CardsDealt:
                    commandLineView.promptForFlip();
                    break;
                case WinnerRevealed:
                    commandLineView.promptForNewGame();
                    break;
            }
        }
    }

    public void addPlayer(String playerName){
        if(gameState == GameState.AddingPlayers){
            players.add(new Player(playerName));
            commandLineView.showPlayerName(players.size(), playerName);
        }
    }

    public void startGame(){
        if(gameState != GameState.CardsDealt){
            deck.shuffle();
            int playerIndex = 1;
            for (Player player : players){
                player.addCardToHand(deck.removeTopCard());
                commandLineView.showFaceDownCardForPlayer(playerIndex++, player.getName());
            }
            gameState = GameState.CardsDealt;
        }
    }

    public void flipCards() {
        int playerIndex = 1;
        for (Player player : players){
            PlayingCard pc = player.getCard(0);
            pc.flip();
            commandLineView.showCardForPlayer(playerIndex++, player.getName(), pc.getRank().toString(), pc.getSuit().toString());
        }
        evaluateWinner();
        displayWinner();
        rebuildDeck();
        gameState = GameState.WinnerRevealed;
    }

    public void restartGame(){
        rebuildDeck();
        gameState = GameState.AddingPlayers;
    }

    void evaluateWinner() {
        winner = evaluator.winnerEvaluate(players);
    }

    void displayWinner() {
        commandLineView.showWinner(winner.getName());
    }

    void rebuildDeck() {
        for (Player player : players){
            deck.returnCardToDeck(player.removeCard());
        }
    }
}
