package package_cardgame_game;

import package_cardgame_controller.GameController;
import package_cardgame_games_logique.HighCardGameEvaluator;
import package_cardgame_model.Deck;
import package_cardgame_view.CommandLineView;
import package_cardgame_view.GameSwing;

public class CardGame {
    public static void main(String[] args) {
        GameSwing gs = new GameSwing();
        gs.createAndShowGUI();

        GameController gc = new GameController(
                            gs,
                            DeckFactory.makeDeck(DeckFactory.DeckType.Normal),
                            new HighCardGameEvaluator()
                        );

        // easy method that launch the thingh
        gc.run();
    }
}
