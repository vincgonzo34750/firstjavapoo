package package_cardgame_game;

import package_cardgame_model.Deck;
import package_cardgame_model.NormalDeck;
import package_cardgame_model.SmallDeck;
import package_cardgame_model.TestDeck;

public class DeckFactory {
    public enum DeckType {
        Normal,
        Small,
        Test
    };

    public static Deck makeDeck(DeckType type){
        switch (type){
            case Normal:return new NormalDeck();
            case Small: return new SmallDeck();
            case Test:return new TestDeck();
        }

        return new NormalDeck();
    }
}
