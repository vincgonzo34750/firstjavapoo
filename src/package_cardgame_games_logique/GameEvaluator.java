package package_cardgame_games_logique;

import package_cardgame_model.Player;
import package_cardgame_model.PlayingCard;

import java.util.List;

public interface GameEvaluator {
    Player winnerEvaluate(List<Player> players);
}
